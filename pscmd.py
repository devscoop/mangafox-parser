#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    See <http://www.gnu.org/licenses/>.
#    Copyright 2012


#imports
import urllib,urllib2,os,sys
from bs import BeautifulSoup


class MangaParser :

	#define an object
	def __init__(self):
		self.base_url = "http://www.mangafox.com/manga/" #the base url
		print "Welcome to MangaFoxParser Version 1.01 CMDLINE"
		
	#if the user decides to proceed, assign variables	
	def initialize(self,manga_name_temp):
		self.manga_name = manga_name_temp.replace(' ','_') #convert the spaces to '_'
		self.chapter_links = []		
		self.source = ""
		self.manga_url = self.base_url+""+self.manga_name	#make the full url	
		self.chapter_names = []
	
	#create directories if they don't exist	
	def dir_make(self,path):
		if os.path.isdir(path) == False: #check if the directory already exists
			try:
            			os.makedirs(path) #if it doesn't, make the directory
           		except OSError, exc:
            			raise #catch any annoying errors. If on Linux, run with sudo

	#return a BS object
	def load_url(self,link):
		return  BeautifulSoup(urllib2.urlopen(link).read()) #conver the url load into a BeautifulSoup object
	
	
	#check if any chapters can be detected
	def does_manga_exist(self):
		self.resource = self.source.findAll('form', attrs={'input' : 'advopts'}) #check if reached default page
		self.chapter_count = 0 		
		load_count = 0
		for check in self.resource:
			load_count+=1
		if load_count == 0:
			self.resource = self.source.findAll('a', attrs={'class' : 'tips'}) #search for chapters
			for chapter in self.resource:
				self.chapter_count+=1 #increment for each chapter
		

	def download_image_from_url(self,link,chapter,count):
		new_image = self.load_url(link).findAll('img', attrs={'id' : 'image'})[0]['src']
		image_name_list = new_image.rpartition(".")
		imgname = str(count) + "." + image_name_list[2]
		imagename = chapter+"/"+imgname #naruto/1/1.jpg
		print "Downloading "+ new_image
		try:
			if os.path.isfile(imagename) == False:
				self.image_download.retrieve(new_image,imagename)
				print "Successful and wrote the image to " + imagename
			else:
				print "You've already downloaded this one.. Moving on"
		except OSError,exc:
			print "There was an error while trying to download " + new_image + ". Moving on ... "		

	#load chapter urls
	def load_chapter_urls(self):
		self.dir_make(self.manga_name)
		self.chapter_count=0
		for chapter in reversed(self.resource):			
			self.chapter_count+=1
			if (self.start_from <= self.chapter_count) and (self.end_at >=self.chapter_count) :
				self.dirname = self.manga_name+"/"+str(self.chapter_count)
				self.dir_make(self.dirname) #make a directory "1"
				self.chapter_links.append(chapter['href'])
				reusable_url = self.load_url(chapter['href']) #load a chapter in a BS object
				self.temp_resource = str(reusable_url.find('select', attrs = {'class' : 'm' } )) #search for the select to find the pages in a chapter url
				number_images = self.temp_resource.count('/') - 1 #count the tags in </option> #remove it for </select>
				absolute_link = chapter['href'].rpartition('/')[0]	#get the portion before /1.html		
				self.image_download = urllib.URLopener()
				self.next_link = chapter['href']
				for x in range(1,number_images):
					self.download_image_from_url(self.next_link,self.dirname,x)
					self.next_link = absolute_link+"/"+ self.load_url(self.next_link).find('img', attrs = {'id' : 'image' } ).parent['href']
			else:
				print "Ignoring chapter : "+str(self.chapter_count)	
		sys.exit(2)

	#main function
	def main(self,argv):
		print "MangaFoxParser Copyright (C)  2012";
		print "This program comes with ABSOLUTELY NO WARRANTY and is meant to demonstrate parsing capabilities of Python with the Beatiful Soup class. This is free software and you are welcome to redistribute it as long as due credit is given to the author(s)";
		manga = argv[1]
		self.start_from = int(argv[2])
		self.end_at = int(argv[3])
		self.initialize(manga)
		#load url
		print "Parsing "+ self.manga_url
		self.source = self.load_url(self.manga_url)
		self.does_manga_exist()
		if self.chapter_count == 0:
			print "The manga could not be found"
		else:
			self.load_chapter_urls()	
manga = MangaParser()
manga.main(sys.argv)
