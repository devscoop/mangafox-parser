## Requires BeautifulSoup - http://www.crummy.com/software/BeautifulSoup/

This project was made for 3 reasons -

1) Got tired of waiting for the ads to load up on MangaFox
2) Got sick of accidently clicking on the last page ads that point to some MMORPG
3) Wanted to learn Python (yes, this is my first project so please excuse the non-adherence towards the Best Practices)

For ps.py,

python ps.py
[manga_name] (on prompt)
[start_at_chapter] (on prompt)
[end_at_chapter] (on prompt)


For pscmd.py,

python pscmd.py [manga_name] [start_at_chapter] [end_at_chapter]




